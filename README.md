# RsaSignDemo

#### 介绍
Rsa 签名验签，回调验签的示例代码

#### 使用说明

1.  回调接口参见 [TestController](https://gitee.com/gsgitee/rsa-sign-demo/blob/master/src/main/java/com/paradise/rsasign/TestController.java)
2.  发起  **创建证书**  请求 参见 test 包下的测试代码 [RsaSignApplicationTests](https://gitee.com/gsgitee/rsa-sign-demo/blob/master/src/test/java/com/paradise/rsasign/RsaSignApplicationTests.java)
3.  生成文件上链格式以及相关方法 参考 [FileHashUtils](https://gitee.com/gsgitee/rsa-sign-demo/blob/master/src/main/java/com/paradise/rsasign/utils/FileHashUtils.java)

注意：            
文件字段格式规范是 `file:[encode url]:[file type]:[hash]:[hash type]`   
支持的 hash type: sha256;  
支持的 file type: pdf/jpg）

文件 url 必须是 Http/Https Url 且支持跨域访问，否则无法完成核验