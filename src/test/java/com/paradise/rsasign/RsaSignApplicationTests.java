package com.paradise.rsasign;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.paradise.rsasign.utils.RsaKeyUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.codec.binary.Base64;
import org.assertj.core.util.Lists;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.util.Base64Utils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.io.File;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Slf4j
@SpringBootTest
class RsaSignApplicationTests {

    @Autowired
    private RestTemplate restTemplate;

    @Test
    void contextLoads() {
    }

    private JSONObject upload(File file) {
        // url
        String uploadUrl = "https://yinji.gaoshan.co/api/gs-enterprise-admin/gs/file/upload";
        // 请求头参数
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.setConnection("Keep-Alive");
        headers.setCacheControl("no-cache");
        headers.add("Authorization", "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTY1NTE4OTE5OCIsInNjb3BlIjpbImFsbCJdLCJpZCI6OCwiZXhwIjoxNjM5MTg5MTg1LCJhdXRob3JpdGllcyI6WyI2X-aZrumAmueUqOaItyJdLCJqdGkiOiIxOGRkMWY1MS02MzhhLTRlOGYtYjdlOS1hOGM1OGNmNDk2NzQiLCJjbGllbnRfaWQiOiJlbnRlcnByaXNlLWFkbWluLWFwcCJ9.aOC_mUnjPwbvD_7jY7TuiA4_WM0Kcuiuqc49yPP-kSfXr-x_P5UrBbjw7id8I849sNwoJB6tIyF6Hpwow_MOIlVX0ErECAlcNPOEJQ2eefrUcmEf-wQ9l5MV_k_j1l7ndC0j4m7GBwfrSde3CcZbWU6myd4CVG3_OF5nmUKRtcQ");
        // 封装请求 body
        FileSystemResource resource = new FileSystemResource(file);
        MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();
        form.add("file", resource);
        HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(form, headers);
        // 发起请求
        ResponseEntity<JSONObject> responseEntity = restTemplate.postForEntity(uploadUrl, httpEntity, JSONObject.class);
        final JSONObject jsonObject = responseEntity.getBody();
        log.info("jsonObject : {} ", jsonObject);
        return jsonObject;
    }

//https://yinji.gaoshan.co/oss/gs2021/202112/3944f3840e49ee379e775c092259ff962afd18a5fb609f7fd79d3349e61133fa.docx
//https://yinji.gaoshan.co/oss/gs2021/202112/2829061363663f67342eefd5c286509f6ed000281d7e5a9cfa60711f47eabc1e.docx
//https://yinji.gaoshan.co/oss/gs2021/202112/66ca54b9d8f24a51281c53ed3c003c087d6eee4ddc2601f26f2174625dfc7606.docx
//https://yinji.gaoshan.co/oss/gs2021/202112/1a64e5467a07a1301142521a9033dde4c89788704fb590cc6bcf5ab5a985ff7c.docx
//https://yinji.gaoshan.co/oss/gs2021/202112/badb15117dc296af9b3e32ff87843b0f907960e7cb9818731111825c65ae7ef2.docx
//https://yinji.gaoshan.co/oss/gs2021/202112/d9c38e3311115d319231201925caf997835d5c08f222555fb73357f8bcf0995b.docx
//https://yinji.gaoshan.co/oss/gs2021/202112/4886095037a4af2345c9a31625145529fb8beae23831328b82b212a15cc8eae3.docx

    @Test
    void testFileC() {
        final long l = System.currentTimeMillis();
        List<String> urls = Lists.list("https://yinji.gaoshan.co/oss/gs2021/202112/3944f3840e49ee379e775c092259ff962afd18a5fb609f7fd79d3349e61133fa.docx",
                "https://yinji.gaoshan.co/oss/gs2021/202112/2829061363663f67342eefd5c286509f6ed000281d7e5a9cfa60711f47eabc1e.docx",
                "https://yinji.gaoshan.co/oss/gs2021/202112/66ca54b9d8f24a51281c53ed3c003c087d6eee4ddc2601f26f2174625dfc7606.docx",
                "https://yinji.gaoshan.co/oss/gs2021/202112/1a64e5467a07a1301142521a9033dde4c89788704fb590cc6bcf5ab5a985ff7c.docx",
                "https://yinji.gaoshan.co/oss/gs2021/202112/badb15117dc296af9b3e32ff87843b0f907960e7cb9818731111825c65ae7ef2.docx",
                "https://yinji.gaoshan.co/oss/gs2021/202112/d9c38e3311115d319231201925caf997835d5c08f222555fb73357f8bcf0995b.docx",
                "https://yinji.gaoshan.co/oss/gs2021/202112/4886095037a4af2345c9a31625145529fb8beae23831328b82b212a15cc8eae3.docx");
        String previewUrl = "https://yinji.gaoshan.co/api/gs-file/preview?url=";
        final ExecutorService executorService = Executors.newFixedThreadPool(8);
        for (String url : urls) {
            log.info("url : {} ", url);
            executorService.submit(() -> {
                final long s1 = System.currentTimeMillis();
                log.info("--- 开始请求: {} ", s1);
                ResponseEntity<JSONObject> responseEntity = restTemplate.postForEntity(previewUrl +
                        Base64Utils.encodeToUrlSafeString(url.getBytes(StandardCharsets.UTF_8)), new HttpEntity<>(""), JSONObject.class);
                final JSONObject jsonObject = responseEntity.getBody();
                if (jsonObject != null) {
                    log.info("res: {}", jsonObject);
                }
                log.info("--- 请求结束，耗时 {}ms", System.currentTimeMillis() - s1);
            });
        }
//        executorService.shutdown();
        try {
            final boolean b = executorService.awaitTermination(2, TimeUnit.MINUTES);
            log.info("awaitTermination : {} ", b);
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
        Assertions.assertTrue(executorService.isShutdown());
        log.info("累计耗时 : {}ms ", System.currentTimeMillis() - l);
    }

    @Test
    void testFile() {
        List<String> urls = new ArrayList<>();

        String uploadUrl = "https://yinji.gaoshan.co/api/gs-enterprise-admin/gs/file/upload";

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        headers.setConnection("Keep-Alive");
        headers.setCacheControl("no-cache");
        headers.add("Authorization", "Bearer eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiIxNTY1NTE4OTE5OCIsInNjb3BlIjpbImFsbCJdLCJpZCI6OCwiZXhwIjoxNjM5MTg5MTg1LCJhdXRob3JpdGllcyI6WyI2X-aZrumAmueUqOaItyJdLCJqdGkiOiIxOGRkMWY1MS02MzhhLTRlOGYtYjdlOS1hOGM1OGNmNDk2NzQiLCJjbGllbnRfaWQiOiJlbnRlcnByaXNlLWFkbWluLWFwcCJ9.aOC_mUnjPwbvD_7jY7TuiA4_WM0Kcuiuqc49yPP-kSfXr-x_P5UrBbjw7id8I849sNwoJB6tIyF6Hpwow_MOIlVX0ErECAlcNPOEJQ2eefrUcmEf-wQ9l5MV_k_j1l7ndC0j4m7GBwfrSde3CcZbWU6myd4CVG3_OF5nmUKRtcQ");


        final ExecutorService executorService = Executors.newFixedThreadPool(4);
        File file = new File("E:\\tempp\\docx\\");
        final File[] files = file.listFiles();
        log.info(Arrays.toString(files));
        assert files != null;
        for (File f : files) {
            executorService.submit(() -> {
                FileSystemResource resource = new FileSystemResource(f);
                MultiValueMap<String, Object> form = new LinkedMultiValueMap<>();
                form.add("file", resource);
                HttpEntity<MultiValueMap<String, Object>> httpEntity = new HttpEntity<>(form, headers);
                ResponseEntity<JSONObject> responseEntity = restTemplate.postForEntity(uploadUrl, httpEntity, JSONObject.class);
                final JSONObject jsonObject = responseEntity.getBody();
                if (jsonObject != null) {
                    log.info("res: {}", jsonObject);
                    if (jsonObject.getInteger("code") == 200) {
                        final String url = jsonObject.getJSONObject("data").getString("url");
                        log.info("url : {} ", url);
                        urls.add(url);
                    }
                }
            });
        }

        executorService.shutdown();
        try {
            final boolean b = executorService.awaitTermination(10, TimeUnit.MINUTES);
            log.info("b : {} ", b);
            for (String url : urls) {
                log.info("url : {} ", url);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
            Thread.currentThread().interrupt();
        }
        Assertions.assertTrue(executorService.isShutdown());
    }

    /**
     * 测试创建证书
     */
    @Test
    void testCredentialsCreate() {
        String url = "http://112.27.216.224:6101";
//        String url = "https://certificate.gaoshan.co:56103";
        String path = "/credential-api/api/credential/create";
        String appId = "c1f935ee66ce40a79172e643d909fc04";
        // 模拟 query 参数
        TreeMap<String, String> queryTreeMap = new TreeMap<>();
        queryTreeMap.put("nonce", "2021");
//        queryTreeMap.put("timestamp", "1627897279335");
        queryTreeMap.put("timestamp", String.valueOf(System.currentTimeMillis()));
        queryTreeMap.put("cptId", "1028");
        queryTreeMap.put("personId", "202100000008");
        queryTreeMap.put("remark", "HelloWorld~");
        queryTreeMap.put("appId", appId);

        String bodyJson = "{" +
                "\"address\": \"长宁大道100号\"," +
                "\"applicationForm\": \"file:http%3a%2f%2f146.56.245.24%3a56082%2fcer.pdf:pdf:7cf647225ed0259b31e9876e4c25ee141827680c82bfa32aa7c01f4dd0c00698:sha256\"," +
                "\"area\": \"高新区\"," +
                "\"businessLicense\": \"file:http%3a%2f%2f146.56.245.24%3a56082%2fcer.pdf:pdf:7cf647225ed0259b31e9876e4c25ee141827680c82bfa32aa7c01f4dd0c00698:sha256\"," +
                "\"contract\": \"file:http%3a%2f%2f146.56.245.24%3a56082%2fcer.pdf:pdf:7cf647225ed0259b31e9876e4c25ee141827680c82bfa32aa7c01f4dd0c00698:sha256\"," +
                "\"contractTemplate\": \"file:http%3a%2f%2f146.56.245.24%3a56082%2fcer.pdf:pdf:7cf647225ed0259b31e9876e4c25ee141827680c82bfa32aa7c01f4dd0c00698:sha256\"," +
                "\"firstCollection\": \"06:00-08:00\"," +
                "\"foodWaste\": \"1000\"," +
                "\"hygieneLicense\": \"file:http%3a%2f%2f146.56.245.24%3a56082%2fcer.pdf:pdf:7cf647225ed0259b31e9876e4c25ee141827680c82bfa32aa7c01f4dd0c00698:sha256\"," +
                "\"oilCollection\": \"06:00-08:00\"," +
                "\"oilWaste\": \"1000\"," +
                "\"responsibility\": \"file:http%3a%2f%2f146.56.245.24%3a56082%2fcer.pdf:pdf:7cf647225ed0259b31e9876e4c25ee141827680c82bfa32aa7c01f4dd0c00698:sha256\"," +
                "\"signNameA\": \"李四\"," +
                "\"signTelA\": \"17722224444\"," +
                "\"signature\": \"file:http%3a%2f%2f146.56.245.24%3a56082%2fcer.pdf:pdf:7cf647225ed0259b31e9876e4c25ee141827680c82bfa32aa7c01f4dd0c00698:sha256\"," +
                "\"specialCollection\": \"06:00-08:00\"," +
                "\"street\": \"长宁街道\"," +
                "\"telA\": \"17722221111\"," +
                "\"telB\": \"17722223333\"}";
//        bodyJson = "{}";
        // 签名串
        String signStr = JSON.toJSONString(queryTreeMap) + bodyJson;

        System.out.println(signStr);

        // 私钥签名
        byte[] signByte = RsaKeyUtil.sign(signStr, Base64.decodeBase64(PkConstant.PRIVATE_KEY));
        String sign = Base64.encodeBase64URLSafeString(signByte);
        // 添加签名参数
        queryTreeMap.put("sign", sign);

        String postUrl = url + path + map2Query(queryTreeMap);
        System.out.println("postUrl_:" + postUrl);
        // 发起请求
        ResponseEntity<String> responseEntity = restTemplate.exchange(
                RequestEntity.post(URI.create(postUrl))
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(bodyJson), String.class);
        System.out.println(responseEntity);
    }

    /**
     * 测试获取 cpt_json
     */
    @Test
    void testCptJson() {
//        String url = "http://112.27.216.224:6101";
        String url = "https://certificate.gaoshan.co:56103";
        String path = "/credential-api/api/cpt/get";
        String appId = "c1f935ee66ce40a79172e643d909fc04";
        // 模拟 query 参数
        TreeMap<String, String> queryTreeMap = new TreeMap<>();
        queryTreeMap.put("nonce", "2021");
        queryTreeMap.put("timestamp", String.valueOf(System.currentTimeMillis()));
        queryTreeMap.put("cptId", "1073");
        queryTreeMap.put("appId", appId);

        // 签名串
        String signStr = JSON.toJSONString(queryTreeMap);
        // 私钥签名
        byte[] signByte = RsaKeyUtil.sign(signStr, Base64.decodeBase64(PkConstant.PRIVATE_KEY));
        String sign = Base64.encodeBase64URLSafeString(signByte);
        // 添加签名参数
        queryTreeMap.put("sign", sign);

        String postUrl = url + path + map2Query(queryTreeMap);
        System.out.println("postUrl_:" + postUrl);
        // 发起请求
        ResponseEntity<String> responseEntity = restTemplate.exchange(RequestEntity.post(URI.create(postUrl))
                .contentType(MediaType.APPLICATION_JSON).build(), String.class);
        assert responseEntity.getStatusCode().equals(HttpStatus.OK);
        log.info("result:{}", responseEntity.getBody());
    }


    private String map2Query(SortedMap<String, String> sortedMap) {
        StringBuilder query = new StringBuilder("?");
        for (Map.Entry<String, String> entry : sortedMap.entrySet()) {
            query.append(entry.getKey()).append("=").append(entry.getValue()).append("&");
        }
        return query.substring(0, query.length() - 1);
    }
}
