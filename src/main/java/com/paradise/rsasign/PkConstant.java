package com.paradise.rsasign;

public class PkConstant {
    private PkConstant() {
        throw new IllegalStateException();
    }

    public static final String PRIVATE_KEY = "MIIBVAIBADANBgkqhkiG9w0BAQEFAASCAT4wggE6AgEAAkEA7WjnCTU9rhLitFbz" +
            "cssCpy4SOBV48U4tzJpr7VGXpOGs4XXIVvCSzZSQan6kL6lJV39jIm2aNQItU8or" +
            "frsrAQIDAQABAkBA7TZ6AzB6IboUPc9YboKsO+JJqj2oKIxH71di0LSbJ4HrMuzs" +
            "zuLynPbJ+FU3mqQ1EROLtb7NHexUAeF0JXh9AiEA9trn/YuNrfYSJScCVnG/4BI2" +
            "xRrEIav5p5Oiu27U2bMCIQD2NGt7WryGdvvoPl7R+k29bqE8kEcfc8FMqUFBYkIm" +
            "ewIgH5RhmU4BEgAo0hfrdKOYqFGsMAr8jFIz3fxFFTVYhYECIGHPaXPUFFmHI4Sc" +
            "YqcgvYmoTb31w4unPP/rrdT/6C/JAiEA8NgVGeNZCeJIkGCUgzgboO+39nZpfNaY" +
            "XzhcWJveuv0=";

    public static final String PUBLIC_KEY = "MFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAO1o5wk1Pa4S4rRW83LLAqcuEjgVePFO" +
            "Lcyaa+1Rl6ThrOF1yFbwks2UkGp+pC+pSVd/YyJtmjUCLVPKK367KwECAwEAAQ==";

    /**
     * 平台公钥
     */
    public static final String PLATFORM_PUB_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAohjXAUrRyfuUbtX1WDH0AQ5U" +
            "DeF7oMSI15RVr/HNyTVk3s4qDfN2mh6VeHAsreTQgtLPRYK9aieLBs1opOOlARXr6BU2Z+HbiZXtQetsQMRI2qSz2YIhzVIe8ygnvyWETZi" +
            "AIyFGDUHPMPhZcTDKsgcc4r77ua+ypo/71dR4xQcr5akQfk3WdufkqB8ZfHbw5vy0WM4fBV6BKkFqhi7X/bdN3jPVzVN52pr8HRNs8r7PGt4k9" +
            "N9nijBaeY12/9llOXeJDiAdHLz6YmukUeSCxQQ5ATK7keQZ2grkhhPXEoaMBM7hZJIKoORkYn/57o0S6hcIZFLrCjKd27lVnab72QIDAQAB";

    public static void main(String[] args) {
        System.out.println(PUBLIC_KEY);
    }
}
