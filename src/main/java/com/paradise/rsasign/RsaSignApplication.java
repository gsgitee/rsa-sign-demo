package com.paradise.rsasign;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RsaSignApplication {

    public static void main(String[] args) {
        SpringApplication.run(RsaSignApplication.class, args);
    }

}
