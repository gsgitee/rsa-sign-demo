package com.paradise.rsasign;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CallBackData {
    private String sign;
    private String timeStamp;
    private String status;
    private String id;

    @Override
    public String toString() {
        return "CallBackData{" +
                "sign='" + sign + '\'' +
                ", timeStamp=" + timeStamp +
                ", status='" + status + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
